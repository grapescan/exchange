package me.grapescan.exchange.model

import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.collections.HashMap

class ConverterImpl(override val currencies: List<Currency>, val ratesProvider: RatesProvider) : Converter {
    override fun convert(value: Money, currency: Currency): Money {
        val rate = getConversionRate(value.currency, currency)
        return Money(rate.multiply(value.amount), currency)
    }

    private var rates = Rates(Currency.getInstance("USD"), HashMap<Currency, BigDecimal>())

    init {
        ratesProvider.loadRates(currencies)
                .subscribeOn(Schedulers.io())
                .subscribe{ rates = it }
    }

    override fun getConversionRate(srcCurrency: Currency, dstCurrency: Currency): BigDecimal = calculateRate(rates, srcCurrency, dstCurrency)

    override fun getConversionRate(srcCurrency: Observable<Currency>, dstCurrency: Observable<Currency>): Observable<BigDecimal> {
        return Observable.combineLatest(
                ratesProvider.loadRates(currencies),
                srcCurrency,
                dstCurrency,
                Function3 { ratesMap, srcCurrValue, dstCurrValue -> calculateRate(ratesMap, srcCurrValue, dstCurrValue) })
    }

    private fun calculateRate(ratesMap: Rates, srcCurrValue: Currency, dstCurrValue: Currency): BigDecimal {
        val baseToSrcRate = ratesMap.entries[srcCurrValue]
        val baseToDstRate = ratesMap.entries[dstCurrValue]
        if (baseToSrcRate != null && baseToDstRate != null) {
            return baseToDstRate.divide(baseToSrcRate, 2, RoundingMode.HALF_UP)
        } else {
            throw IllegalArgumentException("Unsupported currency: ${if (baseToDstRate == null) dstCurrValue else srcCurrValue }")
        }
    }
}


