package me.grapescan.exchange.model

import java.util.*

interface Wallet {
    fun getMoneyAvailable(currency: Currency): Money
}