package me.grapescan.exchange.model.fixer

import android.util.Log
import com.google.gson.GsonBuilder
import com.jakewharton.rx.replayingShare
import io.reactivex.Observable
import me.grapescan.exchange.model.Rates
import me.grapescan.exchange.model.RatesProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class FixerApi : RatesProvider {
    private val TAG = "FixerApi"

    interface ApiServiceInterface {
        @GET("latest")
        fun loadRates(@Query("symbols") currencyCodes: String): Observable<FixerRates>
    }

    private val service : ApiServiceInterface by lazy {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()

        val gson = GsonBuilder()
                .setLenient()
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://api.fixer.io/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build()

        retrofit.create<ApiServiceInterface>(ApiServiceInterface::class.java)
    }

    private var currencies: List<Currency> = emptyList()
    private lateinit var ratesUpdates: Observable<Rates>

    override fun loadRates(currencies: List<Currency>): Observable<Rates> {
        if (this.currencies != currencies) {
            this.currencies = currencies
            val currencyCodes = currencies.map { it.currencyCode }.joinToString(separator = ",") { it }
            ratesUpdates = Observable.interval(30, TimeUnit.SECONDS)
                    .startWith(0) // emit first item immediately
                    .flatMap { tick -> service.loadRates(currencyCodes) }
                    .map { fixerRates -> parse(fixerRates) }
                    .startWith(emptyRates(currencies))
                    .doOnNext { Log.d(TAG, "rates loaded: $it") }
                    .replayingShare()
        }
        return ratesUpdates
    }

    private fun emptyRates(currencies: List<Currency>): Rates {
        val entries = HashMap<Currency, BigDecimal>()
        currencies.forEach { entries.put(it, BigDecimal.ONE) }
        return Rates(currencies[0], entries)
    }

    private fun parse(fixerRates: FixerRates): Rates {
        val base = Currency.getInstance(fixerRates.base)
        val entries = HashMap<Currency,BigDecimal>(fixerRates.rates.size)
        fixerRates.rates.keys.forEach { entries.put(Currency.getInstance(it), fixerRates.rates[it]!!) }
        entries.put(Currency.getInstance(fixerRates.base), BigDecimal.ONE)
        return Rates(base, entries)
    }
}