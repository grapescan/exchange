package me.grapescan.exchange.model

import java.math.BigDecimal
import java.util.*

data class Rates(val base: Currency, val entries: Map<Currency, BigDecimal>)