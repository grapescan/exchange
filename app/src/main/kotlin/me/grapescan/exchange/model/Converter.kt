package me.grapescan.exchange.model

import io.reactivex.Observable
import java.math.BigDecimal
import java.util.*


interface Converter {
    // TODO: remove redundant
    fun getConversionRate(srcCurrency: Observable<Currency>, dstCurrency: Observable<Currency>) : Observable<BigDecimal>
    fun getConversionRate(srcCurrency: Currency, dstCurrency: Currency) : BigDecimal
    val currencies : List<Currency>
    fun convert(value: Money, currency: Currency): Money
}