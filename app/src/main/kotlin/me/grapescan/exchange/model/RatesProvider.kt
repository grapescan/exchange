package me.grapescan.exchange.model

import io.reactivex.Observable
import java.util.*

interface RatesProvider {
    fun loadRates(currencies: List<Currency>): Observable<Rates>
}