package me.grapescan.exchange.model.fixer

import java.math.BigDecimal

data class FixerRates(val base: String, val rates: Map<String, BigDecimal>)