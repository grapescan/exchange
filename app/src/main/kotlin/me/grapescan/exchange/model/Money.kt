package me.grapescan.exchange.model

import java.math.BigDecimal
import java.util.*


data class Money(val amount: BigDecimal, val currency: Currency = Currency.getInstance("USD")) {

    companion object {
        val ZERO = Money(BigDecimal.ZERO)
    }

    override fun toString(): String {
        return "$amount ${currency.currencyCode}"
    }
}