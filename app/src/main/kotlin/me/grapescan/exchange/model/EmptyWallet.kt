package me.grapescan.exchange.model

import java.math.BigDecimal
import java.util.*

class EmptyWallet : Wallet {
    override fun getMoneyAvailable(currency: Currency): Money = Money(BigDecimal.ZERO, currency)
}