package me.grapescan.exchange

import android.app.Application
import android.content.Context
import me.grapescan.exchange.model.*
import me.grapescan.exchange.model.fixer.FixerApi
import me.grapescan.exchange.ui.exchange.*
import me.grapescan.exchange.ui.exchange.money.AmountFormatter
import me.grapescan.exchange.ui.exchange.money.AmountParser
import me.grapescan.exchange.ui.exchange.money.InputParser
import me.grapescan.exchange.ui.exchange.money.OutputFormatter
import java.math.BigDecimal
import java.util.*

class App : Application() {
    object Presenters {
        val exchangePresenter: ExchangeContract.Presenter by lazy {
            ExchangePresenter(Data.converter, Data.wallet)
        }
    }

    // TODO: hide
    object Data {
        val ratesProvider: RatesProvider = FixerApi()

        val converter: Converter by lazy {
            val currencies = listOf(
                    Currency.getInstance("USD"),
                    Currency.getInstance("EUR"),
                    Currency.getInstance("GBP"))
            ConverterImpl(currencies, ratesProvider)
        }

        val wallet: Wallet by lazy {
            EmptyWallet()
        }

        val amountParser: InputParser<String, BigDecimal> = AmountParser()
        val amountFormatter: OutputFormatter<BigDecimal, String> = AmountFormatter()
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
    }

    companion object {
        lateinit var appContext: Context
            private set

        fun getStringRes(resId: Int): String {
            return appContext.getString(resId)
        }
    }
}