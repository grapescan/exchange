package me.grapescan.exchange.ui.base

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<View> {
    private lateinit var viewSubscriptions: CompositeDisposable

    protected fun <T> Observable<T>.subscribeUntilDetached() =
            subscribe().apply { unsubscribeOnDetach(this) }

    protected fun <T> Observable<T>.subscribeUntilDetached(
            onNext: (T) -> Unit) = subscribe(onNext).apply { unsubscribeOnDetach(this) }

    protected fun <T> Observable<T>.subscribeUntilDetached(
            onNext: (T) -> Unit,
            onError: (Throwable) -> Unit) = subscribe(onNext, onError).apply { unsubscribeOnDetach(this) }

    protected fun <T> Observable<T>.subscribeUntilDetached(
            onNext: (T) -> Unit,
            onError: (Throwable) -> Unit,
            onComplete: () -> Unit) = subscribe(onNext, onError, onComplete).apply { unsubscribeOnDetach(this) }

    protected open fun onViewAttached(view: View) {}

    protected open fun onViewDetached(view: View) {}

    protected fun unsubscribeOnDetach(subscription: Disposable) {
        viewSubscriptions.add(subscription)
    }

    fun attach(view: View) {
        viewSubscriptions = CompositeDisposable()
        onViewAttached(view)
    }

    fun detach(view: View) {
        viewSubscriptions.dispose()
        onViewDetached(view)
    }
}