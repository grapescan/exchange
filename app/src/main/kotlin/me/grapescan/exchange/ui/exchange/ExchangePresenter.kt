package me.grapescan.exchange.ui.exchange

import android.util.Log
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.exchange.App
import me.grapescan.exchange.model.Converter
import me.grapescan.exchange.model.Wallet
import me.grapescan.exchange.model.Money
import me.grapescan.exchange.ui.base.Transition
import me.grapescan.exchange.ui.base.transitions
import java.math.BigDecimal
import java.util.*

class ExchangePresenter(val converter: Converter,
                        val wallet: Wallet) : ExchangeContract.Presenter() {
    private val TAG = "ExchangePresenter"
    private var state = ExchangeContract.State.initial()

    private val rateMessageFormatter = { expenseCurr: Currency, incomeCurr: Currency, incomeRate: BigDecimal ->
        "${incomeCurr.symbol} 1 = ${expenseCurr.symbol} ${"%.4f".format(Locale.ROOT, incomeRate)}"
    }
    // TODO: inject formatter
    val moneyMessageFormatter: (Money) -> String = { (value, currency) -> "You have ${currency.symbol} ${"%.2f".format(Locale.ROOT, value)}" }

    override fun onViewAttached(view: ExchangeContract.View) {
        super.onViewAttached(view)
        val networkAvailabilityUpdates: Observable<(ExchangeContract.State) -> ExchangeContract.State> =
                ReactiveNetwork.observeNetworkConnectivity(App.appContext)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map { netState -> { prevState: ExchangeContract.State -> prevState.copy(noNetworkMessageVisible = !netState.isAvailable) } }

        // TODO: extract
        val expenseUpdates: Observable<(ExchangeContract.State) -> ExchangeContract.State> =
                view.expenseUpdates
                        .map { (amount, currency) ->
                            { prevState: ExchangeContract.State ->
                                when {
                                    prevState.expense.currency != currency -> {
                                        Log.d(TAG, "on expense currency update: $currency $amount")
                                        prevState.copy(expense = Money(BigDecimal.ZERO, currency),
                                                income = prevState.income.copy(amount = BigDecimal.ZERO),
                                                expenseRate = converter.getConversionRate(currency, prevState.income.currency),
                                                incomeRate = converter.getConversionRate(prevState.income.currency, currency),
                                                expenseMoneyAvailable = wallet.getMoneyAvailable(currency)
                                        )
                                    }
                                    prevState.expense.amount != amount -> {
                                        Log.d(TAG, "on expense amount update: $currency $amount")
                                        val newExpense = prevState.expense.copy(amount = amount)
                                        prevState.copy(expense = newExpense,
                                                income = converter.convert(newExpense, prevState.income.currency)
                                        )
                                    }
                                    else -> prevState
                                }
                            }
                        }

        val expenseFocusUpdates = view.expenseFocusChanges.map { isFocused ->
            { prevState: ExchangeContract.State ->
                prevState.copy(isExpenseFocused = isFocused,
                        isIncomeFocused = !isFocused)
            }
        }

        val incomeUpdates: Observable<(ExchangeContract.State) -> ExchangeContract.State> =
                view.incomeUpdates
                        .map { (amount, currency) ->
                            { prevState: ExchangeContract.State ->
                                when {
                                    prevState.income.currency != currency -> {
                                        Log.d(TAG, "on income currency update: $currency $amount")
                                        val newIncome = prevState.income.copy(currency = currency)
                                        prevState.copy(expense = converter.convert(newIncome, prevState.expense.currency),
                                                income = newIncome,
                                                expenseRate = converter.getConversionRate(currency, prevState.income.currency),
                                                incomeRate = converter.getConversionRate(prevState.income.currency, currency),
                                                incomeMoneyAvailable = wallet.getMoneyAvailable(currency)
                                        )
                                    }
                                    prevState.income.amount != amount -> {
                                        Log.d(TAG, "on income amount update: $currency $amount")
                                        val newIncome = prevState.income.copy(amount = amount)
                                        prevState.copy(expense = newIncome,
                                                income = converter.convert(newIncome, prevState.expense.currency)
                                        )
                                    }
                                    else -> prevState
                                }
                            }
                        }

        val incomeFocusUpdates = view.incomeFocusChanges.map { isFocused ->
            { prevState: ExchangeContract.State ->
                prevState.copy(isIncomeFocused = isFocused,
                        isExpenseFocused = !isFocused)
            }
        }

        val stateUpdates: Observable<(ExchangeContract.State) -> ExchangeContract.State> = Observable.merge(listOf(
                networkAvailabilityUpdates,
                expenseUpdates,
                expenseFocusUpdates,
                incomeUpdates,
                incomeFocusUpdates
        ))

        val initialTransition = Transition(null, buildViewState(state))

        stateUpdates.scan(state, { currState, stateUpdate -> Log.d(TAG, "new state ${stateUpdate(currState)}"); stateUpdate(currState) })
            .distinctUntilChanged()
            .doOnNext { state = it }
            .map { state -> Log.d(TAG, "new view state ${buildViewState(state)}"); buildViewState(state) }
            .distinctUntilChanged()
            .transitions(initialTransition)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeUntilDetached(
                    onNext = {(src, dst) -> render(view, src, dst) },
                    onError = { t -> Log.e(TAG, "Error", t) }
            )
    }

    // TODO: extract
    private fun render(view: ExchangeContract.View, src: ExchangeContract.View.State?, dst: ExchangeContract.View.State) {
        if (src == null || src.expense != dst.expense) {
            view.showExpense(dst.expense)
        }
        if (src == null || src.income != dst.income) {
            view.showIncome(dst.income)
        }
        if (src == null || src.headerRateMessage != dst.headerRateMessage) {
            view.showHeaderRate(dst.headerRateMessage)
        }
        if (src == null || src.noNetworkMessageVisible != dst.noNetworkMessageVisible) {
            view.setNetworkErrorVisible(dst.noNetworkMessageVisible)
        }
        if (src == null || src.incomeMoneyAvailableMessage != dst.incomeMoneyAvailableMessage) {
            view.showIncomeMoneyAvailableMessage(dst.incomeMoneyAvailableMessage)
        }
        if (src == null || src.expenseMoneyAvailableMessage != dst.expenseMoneyAvailableMessage) {
            view.showExpenseMoneyAvailableMessage(dst.expenseMoneyAvailableMessage)
        }
        if (src == null || src.incomeRateMessage != dst.incomeRateMessage) {
            view.showIncomeRateMessage(dst.incomeRateMessage)
        }
        if (src == null || src.expenseRateMessage != dst.expenseRateMessage) {
            view.showExpenseRateMessage(dst.expenseRateMessage)
        }

    }

    private fun buildViewState(state: ExchangeContract.State): ExchangeContract.View.State {
        val expenseRateMessage = rateMessageFormatter(state.expense.currency, state.income.currency, state.expenseRate)
        val incomeRateMessage = rateMessageFormatter(state.income.currency, state.expense.currency, state.incomeRate)
        return ExchangeContract.View.State(
                income = state.income,
                expense = state.expense,
                headerRateMessage = if (state.isIncomeFocused) incomeRateMessage else expenseRateMessage,
                noNetworkMessageVisible = state.noNetworkMessageVisible,
                incomeMoneyAvailableMessage = moneyMessageFormatter(state.incomeMoneyAvailable),
                expenseMoneyAvailableMessage = moneyMessageFormatter(state.expenseMoneyAvailable),
                incomeRateMessage = if (state.isExpenseFocused) incomeRateMessage else "",
                expenseRateMessage = if (state.isIncomeFocused) expenseRateMessage else ""
        )
    }


}
