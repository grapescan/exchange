package me.grapescan.exchange.ui.exchange.money

interface InputParser<In, Out> {
    fun parse(input: In) : Out
}