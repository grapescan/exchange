package me.grapescan.exchange.ui.exchange.money

import android.content.Context
import android.support.annotation.UiThread
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.widget.LinearLayout
import android.view.LayoutInflater
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.focusChanges
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import me.grapescan.exchange.R
import kotlinx.android.synthetic.main.view_currency.view.*
import me.grapescan.exchange.App
import me.grapescan.exchange.model.Money
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

// TODO: extract presenter
class MoneyView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {
    private val TAG = "MoneyView#${hashCode()}"
    private val moneyUpdates: PublishSubject<Money> = PublishSubject.create()
    private val moneyInput: PublishSubject<Money> = PublishSubject.create()

    private var value = Money.ZERO // TODO: get rid of state
    private val currencies = App.Data.converter.currencies

    init {
        LayoutInflater.from(context).inflate(R.layout.view_currency, this, true)

        moneyInput.observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.d(TAG, "input $it")
                    value = it
                    refresh()
                }

        val amountUpdates = amountView.textChanges().skipInitialValue()
                .debounce(400, TimeUnit.MILLISECONDS)
                .filter { !TextUtils.isEmpty(it) && amountView.hasFocus() && amountView.tag == null}
                .map { BigDecimal(it.toString().toDouble()) }
                .map { value.copy(amount = it) }
                .doOnNext { value = it }

        val currencyUpdates = currencyView.clicks()
                .map { getNextCurrency() }
                .map { value.copy(currency = it) }
                .doOnNext { value = it }

        Observable.merge(amountUpdates, currencyUpdates)
                .distinctUntilChanged()
                .subscribe(moneyUpdates)
    }

    private fun getNextCurrency(): Currency {
        val currentIndex = currencies.indexOf(value.currency)
        val nextIndex = (currentIndex + 1) % currencies.size
        return currencies[nextIndex]
    }

    @UiThread
    private fun refresh() {
        /*
         here we use a hack described at https://stackoverflow.com/a/25751207/252561
         to filter out text changes triggered by setText
         */
        amountView.tag = "Robot is typing"
        amountView.setText(value.amount.toString())
        amountView.tag = null
        currencyView.setText(value.currency.currencyCode)
    }

    fun updates(): Observable<Money> = moneyUpdates

    fun input(): Observer<Money> = moneyInput

    fun focusChanges(): Observable<Boolean> = amountView.focusChanges()

    fun setCurrencyHint(value: String) {
        currencyHintView.setText(value)
    }

    fun setAmountHint(value: String) {
        amountHintView.setText(value)
    }
}