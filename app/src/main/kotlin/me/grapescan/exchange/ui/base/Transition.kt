package me.grapescan.exchange.ui.base

import io.reactivex.Observable

data class Transition<T>(val src: T?, val dst: T)

fun <T : Any> Observable<T>.transitions(seed: Transition<T>): Observable<Transition<T>> =
        this.scan(seed, { b: Transition<T>, elem: T -> Transition(b.dst, elem) })