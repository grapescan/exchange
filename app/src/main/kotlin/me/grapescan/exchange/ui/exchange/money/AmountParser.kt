package me.grapescan.exchange.ui.exchange.money

import java.math.BigDecimal

class AmountParser : InputParser<String, BigDecimal> {
    override fun parse(input: String): BigDecimal {
        try {
            return BigDecimal(input.replace(",", ""))
        } catch (t: Throwable) {
            return BigDecimal.ZERO
        }
    }
}