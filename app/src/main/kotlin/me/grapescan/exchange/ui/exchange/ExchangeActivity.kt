package me.grapescan.exchange.ui.exchange

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.Menu
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_exchange.*
import me.grapescan.exchange.App
import me.grapescan.exchange.R
import me.grapescan.exchange.model.Money

class ExchangeActivity : AppCompatActivity(), ExchangeContract.View {
    // TODO: make view reactive
    override fun showExpense(value: Money) {
        expenseValues.onNext(value)
    }

    override fun showIncome(value: Money) {
        incomeValues.onNext(value)
    }

    override fun showHeaderRate(value: String) {
        headerRateView.setText(value)
    }

    override fun setNetworkErrorVisible(isVisible: Boolean) {
        if (isVisible && !networkError.isShown) {
            networkError.show()
        }
        if (!isVisible && networkError.isShown) {
            networkError.dismiss()
        }
    }

    override fun showExpenseMoneyAvailableMessage(value: String) {
        expenseView.setCurrencyHint(value)
    }

    override fun showIncomeMoneyAvailableMessage(value: String) {
        incomeView.setCurrencyHint(value)
    }

    override fun showExpenseRateMessage(value: String) {
        expenseView.setAmountHint(value)
    }

    override fun showIncomeRateMessage(value: String) {
        incomeView.setAmountHint(value)
    }

    private val TAG = "ExchangeActivity"
    private val presenter: ExchangeContract.Presenter = App.Presenters.exchangePresenter
    private val networkError: Snackbar by lazy {
        Snackbar.make(findViewById(android.R.id.content), R.string.error_no_network, Snackbar.LENGTH_INDEFINITE)
    }

    private val incomeValues = PublishSubject.create<Money>()
    private val expenseValues = PublishSubject.create<Money>()

    override val expenseUpdates: Observable<Money> by lazy {
        expenseView.updates()
    }

    override val incomeUpdates: Observable<Money> by lazy {
        incomeView.updates()
    }

    override val expenseFocusChanges: Observable<Boolean> by lazy {
        expenseView.focusChanges()
    }

    override val incomeFocusChanges: Observable<Boolean> by lazy {
        incomeView.focusChanges()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange)
        initToolbar()

        incomeValues.subscribe(incomeView.input())
        expenseValues.subscribe(expenseView.input())
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        toolbar.title = ""
        toolbar.setNavigationOnClickListener { v -> onBackPressed() }
        toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.inflateMenu(R.menu.exchange_actions)
    }

    override fun onStart() {
        super.onStart()
        presenter.attach(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.detach(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.exchange_actions, menu)
        return true
    }
}
