package me.grapescan.exchange.ui.exchange.money

interface OutputFormatter<In, Out> {
    fun format(input: In) : Out
}