package me.grapescan.exchange.ui.exchange

import io.reactivex.Observable
import me.grapescan.exchange.model.Money
import me.grapescan.exchange.ui.base.BasePresenter
import java.math.BigDecimal

interface ExchangeContract {

    interface View {

        data class State(
                val income: Money,
                val expense: Money,
                val headerRateMessage: String,
                val noNetworkMessageVisible: Boolean,
                val incomeMoneyAvailableMessage: String,
                val expenseMoneyAvailableMessage: String,
                val incomeRateMessage: String,
                val expenseRateMessage: String
        )

        val expenseFocusChanges: Observable<Boolean>
        val incomeFocusChanges: Observable<Boolean>
        val expenseUpdates: Observable<Money>
        val incomeUpdates: Observable<Money>
        fun showExpense(value: Money)
        fun showIncome(value: Money)
        fun showHeaderRate(value: String)
        fun setNetworkErrorVisible(isVisible: Boolean)
        fun showIncomeMoneyAvailableMessage(value: String)
        fun showExpenseMoneyAvailableMessage(value: String)
        fun showIncomeRateMessage(value: String)
        fun showExpenseRateMessage(value: String)
    }

    data class State(
            val income: Money,
            val expense: Money,
            val incomeRate: BigDecimal,
            val expenseRate: BigDecimal,
            val incomeMoneyAvailable: Money,
            val expenseMoneyAvailable: Money,
            val isIncomeFocused: Boolean,
            val isExpenseFocused: Boolean,
            val noNetworkMessageVisible: Boolean) {
        companion object {
            fun initial(): State = State(Money.ZERO, Money.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, Money.ZERO, Money.ZERO, false, false, false)
        }
    }

    abstract class Presenter : BasePresenter<View>()
}