package me.grapescan.exchange.ui.exchange.money

import java.math.BigDecimal
import java.util.*

class AmountFormatter : OutputFormatter<BigDecimal, String> {

    override fun format(input: BigDecimal): String {
        val amountString = "%.2f".format(Locale.ROOT, input)
        return trimZeros(amountString)
    }

    private fun trimZeros(s: String): String = s.replaceFirst("^0+(?!$)", "")
}