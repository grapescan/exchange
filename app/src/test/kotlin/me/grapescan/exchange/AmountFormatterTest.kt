package me.grapescan.exchange

import me.grapescan.exchange.ui.exchange.money.AmountFormatter
import org.amshove.kluent.shouldEqualTo
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal

class AmountFormatterTest {
    private lateinit var formatter: AmountFormatter

    @Before
    fun setUp() {
        formatter = AmountFormatter()
    }

    @Test
    fun handlesIntegers() {
        formatter.format(BigDecimal(9274.0)).shouldEqualTo("9274.00")
    }

    @Test
    fun handlesShortFractions() {
        formatter.format(BigDecimal(9274.6)).shouldEqualTo("9274.60")
    }

    @Test
    fun handlesLongFractions() {
        formatter.format(BigDecimal(9274.6351123)).shouldEqualTo("9274.64")
    }
}