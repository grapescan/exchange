package me.grapescan.exchange

import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import me.grapescan.exchange.model.Converter
import me.grapescan.exchange.model.ConverterImpl
import me.grapescan.exchange.model.Rates
import me.grapescan.exchange.model.RatesProvider
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class ConverterTest {
    private val RUR = Currency.getInstance("RUR")
    private val USD = Currency.getInstance("USD")
    private val EUR = Currency.getInstance("EUR")
    private val GBP = Currency.getInstance("GBP")
    private val rates = Rates(EUR, mapOf(USD to BigDecimal(1.18), EUR to BigDecimal(1.0), GBP to BigDecimal(0.9)))

    private lateinit var converter: Converter

    @Before
    fun setUp() {
        val ratesProvider = object : RatesProvider {
            override fun loadRates(currencies: List<Currency>): Observable<Rates> = Observable.just(rates)
        }
        converter = ConverterImpl(listOf(USD, EUR, GBP), ratesProvider)
    }

    @Test
    fun convertsNonBaseToBase() {
        val testObserver = TestObserver<BigDecimal>()
        converter.getConversionRate(Observable.just(USD), Observable.just(EUR))
                .subscribe(testObserver)
        testObserver.assertNoErrors()
                .assertValue { it == BigDecimal(0.85).setScale(2, RoundingMode.HALF_UP) }
    }

    @Test
    fun convertsBaseToNonBase() {
        val testObserver = TestObserver<BigDecimal>()
        converter.getConversionRate(Observable.just(EUR), Observable.just(GBP))
                .subscribe(testObserver)
        testObserver.assertNoErrors()
                .assertValue{ it == BigDecimal(0.90).setScale(2, RoundingMode.HALF_UP) }
    }

    @Test
    fun convertsNonBaseToNonBase() {
        val testObserver = TestObserver<BigDecimal>()
        converter.getConversionRate(Observable.just(USD), Observable.just(GBP))
                .subscribe(testObserver)
        testObserver.assertNoErrors()
                .assertValue{ it == BigDecimal(0.76).setScale(2, RoundingMode.HALF_UP)}
    }

    @Test
    fun throwsOnWrongCurrency() {
        val testObserver = TestObserver<BigDecimal>()
        converter.getConversionRate(Observable.just(RUR), Observable.just(EUR))
                .subscribe(testObserver)
        testObserver.assertError { t -> t is IllegalArgumentException }
    }
}