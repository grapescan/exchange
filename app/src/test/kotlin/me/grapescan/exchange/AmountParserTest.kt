package me.grapescan.exchange

import me.grapescan.exchange.ui.exchange.money.AmountParser
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal

class AmountParserTest {
    private lateinit var parser: AmountParser

    @Before
    fun setUp() {
        parser = AmountParser()
    }

    @Test
    fun handlesIntegers() {
        parser.parse("132").shouldEqual(BigDecimal(132.0))
    }

    @Test
    fun handlesFractions() {
        parser.parse("1.25").shouldEqual(BigDecimal(1.25))
    }

    @Test
    fun handlesEmptyInput() {
        parser.parse("").shouldEqual(BigDecimal(0.0))
    }
}