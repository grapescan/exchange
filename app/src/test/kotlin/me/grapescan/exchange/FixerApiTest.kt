package me.grapescan.exchange

import io.reactivex.observers.TestObserver
import me.grapescan.exchange.model.fixer.FixerApi
import org.junit.Before
import org.junit.Test
import me.grapescan.exchange.model.Rates
import java.util.*

class FixerApiTest {
    private val USD = Currency.getInstance("USD")
    private val EUR = Currency.getInstance("EUR")
    private val GBP = Currency.getInstance("GBP")

    private lateinit var api: FixerApi

    @Before
    fun setUp() {
        api = FixerApi()
    }

    @Test
    fun returnsAllRequestedRates() {
        val ratesObservable = api.loadRates(listOf(USD, EUR, GBP))
        val testObserver = TestObserver<Rates>()
        ratesObservable.subscribe(testObserver)

        testObserver.assertNoErrors()
                .assertValue({ rates -> rates.entries.keys.contains(USD)})
    }
}