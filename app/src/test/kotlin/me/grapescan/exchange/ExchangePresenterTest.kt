package me.grapescan.exchange

import android.util.Log
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import me.grapescan.exchange.model.ConverterImpl
import me.grapescan.exchange.model.Rates
import me.grapescan.exchange.model.RatesProvider
import me.grapescan.exchange.ui.exchange.money.AmountFormatter
import me.grapescan.exchange.ui.exchange.money.AmountParser
import me.grapescan.exchange.ui.exchange.ExchangeContract
import me.grapescan.exchange.ui.exchange.ExchangePresenter
import org.junit.Before
import org.junit.Test
import java.util.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.plugins.RxAndroidPlugins
import me.grapescan.exchange.model.EmptyWallet
import org.amshove.kluent.shouldEqual
import org.amshove.kluent.shouldNotEqual
import org.junit.After
import org.junit.BeforeClass
import org.junit.runner.RunWith
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.api.mockito.PowerMockito
import org.powermock.modules.junit4.PowerMockRunner
import java.math.BigDecimal


@RunWith(PowerMockRunner::class)
@PrepareForTest(Log::class)
class ExchangePresenterTest {

    private val USD = Currency.getInstance("USD")
    private val EUR = Currency.getInstance("EUR")
    private val GBP = Currency.getInstance("GBP")
    private val rates = Rates(EUR, mapOf(USD to BigDecimal(1.18), EUR to BigDecimal(1.0), GBP to BigDecimal(0.9)))

    private val view: ExchangeContract.View = mock<ExchangeContract.View>()

    val expenseAmountObserver = TestObserver<String>()
    val incomeAmountObserver = TestObserver<String>()
    val expenseCurrencyObserver = TestObserver<String>()
    val incomeCurrencyObserver = TestObserver<String>()

    val expenseAmountSubject = PublishSubject.create<String>()
    val incomeAmountSubject = PublishSubject.create<String>()
    val expenseCurrencyValuesSubject = PublishSubject.create<String>()
    val incomeCurrencyValuesSubject = PublishSubject.create<String>()
    val expenseCurrencyClicksSubject = PublishSubject.create<String>()
    val incomeCurrencyClicksSubject = PublishSubject.create<String>()

    private lateinit var presenter: ExchangePresenter


    companion object {
        @BeforeClass
        @JvmStatic
        fun commonSetUp() {
            PowerMockito.mockStatic(Log::class.java)
        }
    }

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

        val ratesProvider = object : RatesProvider {
            override fun loadRates(currencies: List<Currency>): Observable<Rates> = Observable.fromCallable { rates }
        }
        val converter = ConverterImpl(listOf(USD, EUR, GBP), ratesProvider)
        presenter = ExchangePresenter(converter, EmptyWallet(), AmountParser(), AmountFormatter())

        incomeAmountSubject.subscribe(incomeAmountObserver)
        expenseAmountSubject.subscribe(expenseAmountObserver)
        incomeCurrencyValuesSubject.subscribe(incomeCurrencyObserver)
        expenseCurrencyValuesSubject.subscribe(expenseCurrencyObserver)
/*
        whenever(view.expenseAmountValues).thenReturn(expenseAmountSubject)
        whenever(view.incomeAmountValues).thenReturn(incomeAmountSubject)
        whenever(view.expenseCurrencySwitch).thenReturn(expenseCurrencyClicksSubject)
        whenever(view.incomeCurrencySwitch).thenReturn(incomeCurrencyClicksSubject)

        whenever(view.expenseAmountConsumer).thenReturn(Consumer { expenseAmountSubject.onNext(it) })
        whenever(view.incomeAmountConsumer).thenReturn(Consumer { incomeAmountSubject.onNext(it) })
        whenever(view.expenseCurrencyConsumer).thenReturn(Consumer { expenseCurrencyValuesSubject.onNext(it) })
        whenever(view.incomeCurrencyConsumer).thenReturn(Consumer { incomeCurrencyValuesSubject.onNext(it) })*/
        throw RuntimeException("Uncomment the test")

        presenter.onViewAttach(view)
    }

    @After
    fun tearDown() {
        presenter.onViewDetach()
    }

    @Test
    fun setsInitialValues() {
        expenseAmountObserver
                .assertNoErrors()
                .values().last() shouldEqual "0.00"
        incomeAmountObserver
                .assertNoErrors()
                .values().last() shouldEqual "0.00"
        expenseCurrencyObserver
                .assertNoErrors()
                .assertValue("USD")
        incomeCurrencyObserver
                .assertNoErrors()
                .assertValue("EUR")
    }

    @Test
    fun updatesExpenseAmountOnIncomeAmountChange() {
        expenseAmountObserver
                .assertNoErrors()
                .values().last() shouldEqual "0.00"
        incomeAmountSubject.onNext("25.00")
        expenseAmountObserver
                .assertNoErrors()
                .values().last() shouldNotEqual "0.00"
    }

    @Test
    fun updatesIncomeAmountOnExpenseAmountChange() {
        incomeAmountObserver
                .assertNoErrors()
                .values().last() shouldEqual "0.00"
        expenseAmountSubject.onNext("61.34")
        incomeAmountObserver
                .assertNoErrors()
                .values().last() shouldNotEqual "0.00"
    }

    @Test
    fun updatesCurrencyOnSwitch() {
        expenseCurrencyObserver
                .assertNoErrors()
                .assertValue("USD")
        incomeCurrencyObserver
                .assertNoErrors()
                .assertValue("EUR")

        expenseCurrencyClicksSubject.onNext("USD")
        expenseCurrencyObserver
                .assertNoErrors()
                .values().last() shouldNotEqual  "USD"

        incomeCurrencyClicksSubject.onNext("EUR")
        incomeCurrencyObserver
                .assertNoErrors()
                .values().last() shouldNotEqual "EUR"
    }

    @Test
    fun resetsAmountsOnCurrencyChange() {
        incomeAmountSubject.onNext("42.00")
        expenseAmountSubject.onNext("51.88")

        incomeCurrencyClicksSubject.onNext("GBP")

        incomeAmountObserver
                .assertNoErrors()
                .values().last() shouldEqual "0.00"
        expenseAmountObserver
                .assertNoErrors()
                .values().last() shouldEqual "0.00"
    }

    @Test
    fun persistsState() {
        TODO("Test state persistence after reattaching")
    }
}